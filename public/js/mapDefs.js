mapboxgl.accessToken = 'pk.eyJ1IjoiYmFsa2UxMyIsImEiOiJja3lnNGh5ajIwaGh5Mm5zczlpa2tzN2ZlIn0.f309-nIyBxbb4wqubfmoRg';

const mapDef1 = {
	container: "map1",
    style: 'mapbox://styles/mapbox/light-v10', // style URL
    center: [-99, 31], // starting position [lng, lat]
    zoom: 6.8 // starting zoom
}

const mapDef = {
	container: "map2",
    style: 'mapbox://styles/mapbox/light-v10', // style URL
    center: [-99, 31], // starting position [lng, lat]
    // center: [-105, 38], // starting position [lng, lat]
    zoom: 6.8 // starting zoom
}

// mapbox://styles/mapbox/light-v10
// mapbox://styles/mapbox/streets-v11