// trigger map loading and event listeners at the earliest possible point, but not before
document.addEventListener("DOMContentLoaded", () => {

    const state_lookup = [
        {stusps:"al",statename:"ALABAMA"},
        {stusps:"ak",statename:"ALASKA"},
        {stusps:"az",statename:"ARIZONA"},
        {stusps:"ar",statename:"ARKANSAS"},
        {stusps:"ca",statename:"CALIFORNIA"},
        {stusps:"co",statename:"COLORADO"},
        {stusps:"ct",statename:"CONNECTICUT"},
        {stusps:"de",statename:"DELAWARE"},
        {stusps:"fl",statename:"FLORIDA"},
        {stusps:"ga",statename:"GEORGIA"},
        {stusps:"hi",statename:"HAWAII"},
        {stusps:"id",statename:"IDAHO"},
        {stusps:"il",statename:"ILLINOIS"},
        {stusps:"in",statename:"INDIANA"},
        {stusps:"ia",statename:"IOWA"},
        {stusps:"ks",statename:"KANSAS"},
        {stusps:"ky",statename:"KENTUCKY"},
        {stusps:"la",statename:"LOUISIANA"},
        {stusps:"me",statename:"MAINE"},
        {stusps:"md",statename:"MARYLAND"},
        {stusps:"ma",statename:"MASSACHUSETTS"},
        {stusps:"mi",statename:"MICHIGAN"},
        {stusps:"mn",statename:"MINNESOTA"},
        {stusps:"ms",statename:"MISSISSIPPI"},
        {stusps:"mo",statename:"MISSOURI"},
        {stusps:"mt",statename:"MONTANA"},
        {stusps:"ne",statename:"NEBRASKA"},
        {stusps:"nv",statename:"NEVADA"},
        {stusps:"nh",statename:"NEW HAMPSHIRE"},
        {stusps:"nj",statename:"NEW JERSEY"},
        {stusps:"nm",statename:"NEW MEXICO"},
        {stusps:"ny",statename:"NEW YORK"},
        {stusps:"nc",statename:"NORTH CAROLINA"},
        {stusps:"nd",statename:"NORTH DAKOTA"},
        {stusps:"oh",statename:"OHIO"},
        {stusps:"ok",statename:"OKLAHOMA"},
        {stusps:"or",statename:"OREGON"},
        {stusps:"pa",statename:"PENNSYLVANIA"},
        {stusps:"ri",statename:"RHODE ISLAND"},
        {stusps:"sc",statename:"SOUTH CAROLINA"},
        {stusps:"sd",statename:"SOUTH DAKOTA"},
        {stusps:"tn",statename:"TENNESSEE"},
        {stusps:"tx",statename:"TEXAS"},
        {stusps:"ut",statename:"UTAH"},
        {stusps:"vt",statename:"VERMONT"},
        {stusps:"va",statename:"VIRGINIA"},
        {stusps:"wa",statename:"WASHINGTON"},
        {stusps:"wv",statename:"WEST VIRGINIA"},
        {stusps:"wi",statename:"WISCONSIN"},
        {stusps:"wy",statename:"WYOMING"}
    ];

    const chamber_lookup = [
        { abbreviation: "cong", name: "CONGRESSIONAL" },
        { abbreviation: "sldu", name: "STATE UPPER" },
        { abbreviation: "sldl", name: "STATE LOWER" },
    ]

    const resultSelectState = document.getElementById('select-state').value;
    let statenameDiv = document.getElementById('state-name');
    let stateName = state_lookup.find(({ stusps }) => stusps === resultSelectState);
    let districtsDiv = document.getElementById('districts');
    let demSeatsDiv = document.getElementById('dem-seats');
    let repSeatsDiv = document.getElementById('rep-seats');

    console.log(`districtsDiv value on load: ${districtsDiv.innerHTML}`);
    console.log(`demSeatsDiv value on load: ${demSeatsDiv.innerHTML}`);
    console.log(`repSeatsDiv value on load: ${repSeatsDiv.innerHTML}`);
    console.log(`select-state value on load: ${resultSelectState}`);
    console.log(`stateName value on load: ${stateName.statename}`);

    // Set the state-name div with the full statename
    statenameDiv.innerHTML = stateName.statename;

    const selectElement = document.querySelector('#select-state');
    selectElement.addEventListener('change', (event) => {
        const result = document.querySelector('.state-name');
        console.log(`const result: ${result}`)
        result.textContent = `${event.target.value}`;
        stateName = state_lookup.find(({ stusps }) => stusps === result.textContent);
        statenameDiv.innerHTML = stateName.statename;
        console.log(`stateName from the event listner: ${stateName.statename}`);
        
        renderStateData();

    });


    let chambertypeDiv = document.getElementById('chamber-type');
    const initialChamberResult = document.getElementById('select-chamber').value;
    console.log(`select-chamber value on load: ${initialChamberResult}`)

    const selectElementChamber = document.querySelector('#select-chamber');
    selectElementChamber.addEventListener('change', (event) => {
        const result = document.querySelector('.chamber-type');
        result.textContent = `${event.target.value}`;
        chamberName = chamber_lookup.find(({ abbreviation }) => abbreviation === result.textContent);
        chambertypeDiv.innerHTML = chamberName.name;
        
        console.log(`chamber-name from the event listner: ${result.textContent}`);
    });

    // Read statedata.json
    // fetch('js/statedata.json')
    // .then(response => response.json())
    // .then(data => console.log(data))
    // .catch(error => console.log(error))


    async function getStateData() {
        let url = 'https://kbalke.gitlab.io/dashboard-dev/js/statedata.json';
        try {
            let res = await fetch(url);
            return await res.json();
        } catch (error) {
            console.log(error);
        }
    }

    async function renderStateData() {
        let states = await getStateData();
        let html = '';

        /**get all keys***/
        var keys = Object.keys(states);
        console.log(`keys: ${keys}`)

        console.log(`states.metrics.cnt_cd118: ${states.metrics.cnt_cd118}`)
        console.log(`states.metrics.dem_seats: ${states.metrics.dem_seats}`)
        console.log(`states.metrics.rep_seats: ${states.metrics.rep_seats}`)

        console.log(`states.incumbents.cong_dem: ${states.incumbents.cong_dem}`)
        console.log(`states.incumbents.cong_rep: ${states.incumbents.cong_rep}`)


        console.log(`states.cong.plans: ${states.cong.plans}`)

        states.cong.plans.forEach(plan => console.log(plan))

        districtsDiv.innerHTML = states.metrics.cnt_cd118;
        demSeatsDiv.innerHTML = states.metrics.dem_seats;
        repSeatsDiv.innerHTML = states.metrics.rep_seats;


        // data["address"][field]; 

        // states.forEach(user => {
        //     let htmlSegment = `<div class="user">
        //                         <img src="${user.profileURL}" >
        //                         <h2>${user.firstName} ${user.lastName}</h2>
        //                         <div class="email"><a href="email:${user.email}">${user.email}</a></div>
        //                     </div>`;
    
        //     html += htmlSegment;
        // });
    
        // let container = document.querySelector('.container');
        // container.innerHTML = html;
    }
    
    // renderStateData();



});