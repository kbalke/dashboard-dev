// trigger map loading and event listeners at the earliest possible point, but not before
document.addEventListener("DOMContentLoaded", () => {
    
    // restore the last map center and zoom level
    const coordinatesAndZoom = JSON.parse(localStorage.getItem("currentLocationAndZoom"));

    if (coordinatesAndZoom) {
        mapDef.center = coordinatesAndZoom.coordinates;
        mapDef.zoom = coordinatesAndZoom.zoom;
    }

    map1 = new mapboxgl.Map(mapDef1)

    map = new mapboxgl.Map(mapDef);

/*     var nav_control = new mapboxgl.NavigationControl();
    var full_screen_control = new mapboxgl.FullscreenControl()
    map.addControl(nav_control, 'top-left');
    map.addControl(full_screen_control, 'top-left'); */

    map.addControl(new mapboxgl.NavigationControl());
    map.addControl(new mapboxgl.FullscreenControl());

    // variable to control the hoverState
    let hoveredStateId = null;

    map.on("load", function () {
        //  sources
        map.addSource('plan_test', {
            type: 'geojson',
            tolerance: 2,
            // data: 'https://kbalke.gitlab.io/dashboard-dev/js/districts.geojson'
            data: 'https://kbalke.gitlab.io/dashboard-dev/js/tx_cong_2012_enacted.geojson'
        });

        // Add a new layer to visualize the polygon.
        map.addLayer({
            'id': 'plan_test',
            'type': 'fill',
            'source': 'plan_test', // reference the data source
            'layout': {},
            'paint': {
                "fill-color": [
                    'interpolate',
                    ['linear'],
                    ['get', 'pres20demperf'],
                    0,
                    '#ff0000',
                    44,
                    '#ff7f7f',
                    48,
                    '#ffbebe',
                    50,
                    '#bed2ff',
                    52,
                    '#55aaff',
                    56,
                    '#0070ff'
                ],
                'fill-opacity': [
                    'case',
                    ['boolean', ['feature-state', 'hover'], false],
                    0.5,
                    0.8
                ]
            }
        });


        // Add a black outline around the polygon.
        map.addLayer({
            'id': 'outline',
            'type': 'line',
            'source': 'plan_test',
            'layout': {},
            'paint': {
                'line-color': '#ffffff',
                'line-width': 1
            }
        });

        // Create a popup, but don't add it to the map yet.
        const popup = new mapboxgl.Popup({
            closeButton: false,
            closeOnClick: false
        });

        // Convert number to include thousands separator
        function separator(numb) {
            var str = numb.toString().split(".");
            str[0] = str[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return str.join(".");
        }

        map.on('mousemove', 'plan_test', (e) => {
            // Change the cursor style as a UI indicator.
            map.getCanvas().style.cursor = 'pointer';

            // convert the tot_pop field to include thousands separator
            const tot_pop = separator(e.features[0].properties.tot_pop)
            const voting_pop = separator(e.features[0].properties.voting_pop)
            const cvap_pop = separator(e.features[0].properties.cvap_pop)

            // build the description for the district pop-up
            const description = `
            <br><h3>District ${e.features[0].properties.district}</h3>
            <h2><strong>Colorado Congressional Enacted</strong></h2>
            <p>Total Population: ${tot_pop}</p>
            <p>Voting Population: ${voting_pop}</p>
            <p>Citizen Voting Population: ${cvap_pop}</p>
            <p>Presidenatial 2020 Democratic Performance: ${e.features[0].properties.pres20demperf}</p>
            <p>${e.features[0].properties.incumbent}</p>
            `

            // Single out the first found feature.
            var feature = e.features[0];

            // Display a popup 
            popup.setLngLat(e.lngLat)
                .setHTML(description)
                .addTo(map);

            if (e.features.length > 0) {
                if (hoveredStateId !== null) {
                    map.setFeatureState(
                        { source: 'plan_test', id: hoveredStateId },
                        { hover: false }
                    );
                }
                hoveredStateId = e.features[0].id;
                map.setFeatureState(
                    { source: 'plan_test', id: hoveredStateId },
                    { hover: true }
                );
            }


        });

        map.on('mouseleave', 'plan_test', () => {
            map.getCanvas().style.cursor = '';
            popup.remove();

            if (hoveredStateId !== null) {
                map.setFeatureState(
                    { source: 'plan_test', id: hoveredStateId },
                    { hover: false }
                );
            }
            hoveredStateId = null;

        });

    });


    // After the last frame rendered before the map enters an "idle" state.
    map.on('idle', () => {
        // If these two layers were not added to the map, abort
        if (!map.getLayer('plan_test')) {
            return;
        }

        // Enumerate ids of the layers.
        const toggleableLayerIds = ['plan_test'];

        // Set up the corresponding toggle button for each layer.
        for (const id of toggleableLayerIds) {
            // Skip layers that already have a button set up.
            if (document.getElementById(id)) {
                continue;
            }

            // Create a link.
            const link = document.createElement('a');
            link.id = id;
            link.href = '#';
            link.textContent = id;
            link.className = 'active';

            // Show or hide layer when the toggle is clicked.
            link.onclick = function (e) {
                const clickedLayer = this.textContent;
                e.preventDefault();
                e.stopPropagation();

                const visibility = map.getLayoutProperty(
                    clickedLayer,
                    'visibility'
                );

                // Toggle layer visibility by changing the layout object's visibility property.
                if (visibility === 'visible') {
                    map.setLayoutProperty(clickedLayer, 'visibility', 'none');
                    this.className = '';
                } else {
                    this.className = 'active';
                    map.setLayoutProperty(
                        clickedLayer,
                        'visibility',
                        'visible'
                    );
                }
            };

            const layers = document.getElementById('menu');
            layers.appendChild(link);
        }
    });

    map.on("move", () => {

        const currentZoom = map.getZoom();

        document.getElementById("zoomLevelDiv").innerHTML = currentZoom.toFixed(1);

        let coordinatesAndZoom = {};

        const currentCenter = map.getCenter();
        coordinatesAndZoom.coordinates = [currentCenter.lng, currentCenter.lat];
        coordinatesAndZoom.zoom = currentZoom;

        localStorage.setItem("currentLocationAndZoom", JSON.stringify(coordinatesAndZoom));

        console.log(JSON.parse(localStorage.getItem("currentLocationAndZoom")));

    });


});
